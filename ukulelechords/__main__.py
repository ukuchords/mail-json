import os
import sys

from fretboard import UkuleleChord


def read_from_stdin():
    stdin_text = ''
    for line in sys.stdin:
        stdin_text = stdin_text + line
    return stdin_text


def read_from_file(file2read_name):

    print(os.path.abspath(file2read_name))
    with open(file2read_name, 'r', encoding="utf-8") as file2read:
        content = file2read.read()
    return content


def main() -> int:

    os.makedirs("ukulele", exist_ok=True)

    UkuleleChord(positions='2100', fingers='21--').save('ukulele/A.svg')
    UkuleleChord(positions='2000', fingers='2---').save('ukulele/Am.svg')
    UkuleleChord(positions='0100', fingers='-1--').save('ukulele/A7.svg')
    UkuleleChord(positions='0000', fingers='----').save('ukulele/Am7.svg')
    UkuleleChord(positions='2342', fingers='1341').save('ukulele/Asus2.svg')
    UkuleleChord(positions='2200', fingers='23--').save('ukulele/Asus4.svg')
    UkuleleChord(positions='02400', fingers='13--').save('ukulele/A5.svg')

    UkuleleChord(positions='3211', fingers='3211').save('ukulele/Bb.svg')
    UkuleleChord(positions='3111', fingers='3111').save('ukulele/Bbm.svg')
    UkuleleChord(positions='1211', fingers='1211').save('ukulele/Bb7.svg')
    UkuleleChord(positions='2452', fingers='1341').save('ukulele/Bbm7.svg')
    UkuleleChord(positions='3011', fingers='3-11').save('ukulele/Bbsus2.svg')
    UkuleleChord(positions='3311', fingers='3411').save('ukulele/Bbsus4.svg')
    UkuleleChord(positions='3013', fingers='1-23').save('ukulele/Bb5.svg')

    UkuleleChord(positions='4322', fingers='3211').save('ukulele/B.svg')
    UkuleleChord(positions='4222', fingers='3111').save('ukulele/Bm.svg')
    UkuleleChord(positions='2322', fingers='1211').save('ukulele/B7.svg')
    UkuleleChord(positions='2222', fingers='1111').save('ukulele/Bm7.svg')
    UkuleleChord(positions='4122', fingers='4123').save('ukulele/Bsus2.svg')
    UkuleleChord(positions='4422', fingers='3411').save('ukulele/Bsus4.svg')
    UkuleleChord(positions='0x22', fingers='--12').save('ukulele/B5.svg')

    UkuleleChord(positions='0003', fingers='---3').save('ukulele/C.svg')
    UkuleleChord(positions='0333', fingers='-111').save('ukulele/Cm.svg')
    UkuleleChord(positions='0001', fingers='---1').save('ukulele/C7.svg')
    UkuleleChord(positions='3333', fingers='1111').save('ukulele/Cm7.svg')
    UkuleleChord(positions='0233', fingers='-123').save('ukulele/Csus2.svg')
    UkuleleChord(positions='0013', fingers='--13').save('ukulele/Csus4.svg')
    UkuleleChord(positions='0033', fingers='--11').save('ukulele/C5.svg')
    UkuleleChord(positions='0203', fingers='-2-3').save('ukulele/Cadd9.svg')

    UkuleleChord(positions='1114', fingers='1114').save('ukulele/C#.svg')
    UkuleleChord(positions='1444', fingers='1444').save('ukulele/C#m.svg')
    UkuleleChord(positions='1112', fingers='1112').save('ukulele/C#7.svg')
    UkuleleChord(positions='4444', fingers='1111').save('ukulele/C#m7.svg')
    UkuleleChord(positions='1344', fingers='1234').save('ukulele/C#sus2.svg')
    UkuleleChord(positions='1344', fingers='1234').save('ukulele/C#sus2.svg')
    UkuleleChord(positions='1124', fingers='1124').save('ukulele/C#sus4.svg')
    UkuleleChord(positions='1144', fingers='1134').save('ukulele/C#5.svg')

    UkuleleChord(positions='2220', fingers='123-').save('ukulele/D.svg')
    UkuleleChord(positions='2210', fingers='231-').save('ukulele/Dm.svg')
    UkuleleChord(positions='2223', fingers='1112').save('ukulele/D7.svg')
    UkuleleChord(positions='2213', fingers='2314').save('ukulele/Dm7.svg')
    UkuleleChord(positions='2200', fingers='23--').save('ukulele/Dsus2.svg')
    UkuleleChord(positions='0230', fingers='-23-').save('ukulele/Dsus4.svg')
    UkuleleChord(positions='22x0', fingers='23--').save('ukulele/D5.svg')
    UkuleleChord(positions='2425', fingers='1314').save('ukulele/Dadd9.svg')

    UkuleleChord(positions='0331', fingers='-341').save('ukulele/D#.svg')
    UkuleleChord(positions='3321', fingers='3421').save('ukulele/D#m.svg')
    UkuleleChord(positions='3334', fingers='1112').save('ukulele/D#7.svg')
    UkuleleChord(positions='3324', fingers='2314').save('ukulele/D#m7.svg')
    UkuleleChord(positions='3311', fingers='3411').save('ukulele/D#sus2.svg')
    UkuleleChord(positions='1341', fingers='1341').save('ukulele/D#sus4.svg')
    UkuleleChord(positions='3366', fingers='1134').save('ukulele/D#5.svg')

    UkuleleChord(positions='4442', fingers='2341').save('ukulele/E.svg')
    UkuleleChord(positions='0432', fingers='-321').save('ukulele/Em.svg')
    UkuleleChord(positions='1202', fingers='12-3').save('ukulele/E7.svg')
    UkuleleChord(positions='0202', fingers='-2-3').save('ukulele/Em7.svg')
    UkuleleChord(positions='4422', fingers='3411').save('ukulele/Esus2.svg')
    UkuleleChord(positions='2342', fingers='1341').save('ukulele/Esus4.svg')
    UkuleleChord(positions='4402', fingers='34-1').save('ukulele/E5.svg')

    UkuleleChord(positions='2010', fingers='2-1-').save('ukulele/F.svg')
    UkuleleChord(positions='1013', fingers='1-24').save('ukulele/Fm.svg')
    UkuleleChord(positions='2313', fingers='2314').save('ukulele/F7.svg')
    UkuleleChord(positions='1313', fingers='1314').save('ukulele/Fm7.svg')
    UkuleleChord(positions='0013', fingers='--13').save('ukulele/Fsus2.svg')
    UkuleleChord(positions='3011', fingers='3-11').save('ukulele/Fsus4.svg')
    UkuleleChord(positions='x013', fingers='--13').save('ukulele/F5.svg')
    UkuleleChord(positions='0010', fingers='--1-').save('ukulele/Fadd9.svg')

    UkuleleChord(positions='0232', fingers='-132').save('ukulele/G.svg')
    UkuleleChord(positions='0231', fingers='-231').save('ukulele/Gm.svg')
    UkuleleChord(positions='0212', fingers='-213').save('ukulele/G7.svg')
    UkuleleChord(positions='0211', fingers='-211').save('ukulele/Gm7.svg')
    UkuleleChord(positions='0230', fingers='-23-').save('ukulele/Gsus2.svg')
    UkuleleChord(positions='0233', fingers='-123').save('ukulele/Gsus4.svg')
    UkuleleChord(positions='x235', fingers='-124').save('ukulele/G5.svg')
    UkuleleChord(positions='0252', fingers='-142').save('ukulele/Gadd9.svg')

    return 0


if __name__ == "__main__":
    sys.exit(main())
