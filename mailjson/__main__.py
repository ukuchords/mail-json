import argparse
import json
import shlex
import sys
import os

from mail_reader import MailReader


def main() -> int:
    """Echo the input arguments to standard output"""
    phrase = shlex.join(sys.argv)

    parser = argparse.ArgumentParser(prog='mailjson',
                                     description='Read unseen Mails  from Server and '
                                                 'convert them to json with base64 coded entries')
    parser.add_argument('--mailserver', nargs=1, required=True, help='Mail server to read from')
    parser.add_argument('--username', nargs=1, required=True, help='Username to access mail server')
    parser.add_argument('--password', nargs=1, required=True, help='Password to access mail server')
    parser.add_argument('--target_file', nargs=1, required=False, help='File to write result')

    args = parser.parse_args()
    mailserver = args.mailserver[0]
    username = args.username[0]
    password = args.password[0]
    target_file_name = args.target_file[0]  if args.target_file else None

    reader = MailReader(username, password, mailserver)
    mail_result = reader.read_mails()
    mail2json_text = json.dumps(mail_result)
    if target_file_name:
        print(os.path.abspath(target_file_name))
        with open(target_file_name, "w",  encoding='utf8', newline='\n') as target_file:
            target_file.write(mail2json_text)
    else:
        print(mail2json_text)

    return 0


if __name__ == "__main__":
    sys.exit(main())
