import imaplib
import email
from email.header import decode_header
import base64


class MailReader:

    def read_mails(self):
        imap = imaplib.IMAP4_SSL(self.imap_server)
        imap.login(self.username, self.password)

        status, messages = imap.select("INBOX", readonly=False)

        obj, data = imap.search(None, 'UNSEEN')
        mails_results = []

        for num in data[0].split():
            # fetch the email message by ID
            res, response = imap.fetch(num, "(RFC822)")
            # parse a bytes email into a message object
            msg = email.message_from_bytes(response[0][1])
            mail_fields = self.__mail_message_fields(msg)
            mails_results.append(mail_fields)

            imap.store(num, '-FLAGS', '\\Seen')

        return mails_results

    def __clean(self, text):
        # clean text for creating a folder
        return "".join(c if c.isalnum() else "_" for c in text)

    def __mail_message_fields(self, msg):

        # decode the email subject
        subject, encoding = decode_header(msg["Subject"])[0]
        if isinstance(subject, bytes):
            # if it's a bytes, decode to str
            subject = subject.decode(encoding)
        # decode email sender
        from_field, encoding = decode_header(msg.get("From"))[0]
        if isinstance(from_field, bytes):
            if encoding is not None:
                from_field = from_field.decode(encoding)
            else:
                from_field = ''

        content_parts = self.__mail_content(msg)
        return {
            "subject": self.to_base64(subject),
            "from": self.to_base64(from_field),
            "parts": content_parts
        }

    def decode_mime_words(self, s):
        return u''.join(
            word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
            for word, encoding in email.header.decode_header(s))

    def to_base64(self, string2convert):
        return base64.b64encode(string2convert.encode()).decode()

    def encode_body(self, body, content_type, charset):
        if content_type.startswith('text'):
            utf8_string= body.decode(charset)
            base64_byte= base64.b64encode(utf8_string.encode())
            return base64_byte.decode()
        else:
            return  base64.b64encode(body).decode() # byte to string

    def __mail_content(self, msg):
        content_parts = []
        if msg.is_multipart():
            # iterate over email parts
            for part in msg.walk():
                # extract content of email
                try:
                    if part.is_multipart():  # seems not to be necessary, alternate parts
                        for subpart in part.get_payload():
                            if subpart.is_multipart():
                                for subsubpart in subpart.get_payload():
                                    branch = "   subsubpart"
                    else:
                        filename = "" if part.get_filename() is None else self.decode_mime_words(part.get_filename())
                        body = part.get_payload(decode=True)
                        content_parts.append({"level": "part",
                                              "content_type": part.get_content_type(),
                                              "payload": self.encode_body(body, part.get_content_type(), part.get_content_charset()),
                                              "filename": self.to_base64(filename)})

                except Exception as excp:
                    print(type(excp))  # the exception type
                    # print(excp.args)     # arguments stored in .args
                    # print(excp)          # __str__ allows args to be printed directly,

        else:
            # extract content type of email
            content_type = msg.get_content_type()
            if content_type == "text/plain":
                # print only text email parts
                # get the email body
                body = msg.get_payload(decode=True)
                charset = msg.get_charset()
                body_unencoded = msg.get_payload(decode=False)
                content_parts.append({"level": "msg",
                                      "content_type": content_type,
                                      "payload": self.encode_body(body, msg.get_content_type(), msg.get_content_charset()),
                                      "filename": self.to_base64("")})
        return content_parts

    def __init__(self, username, password, server):
        self.username = username
        self.password = password
        self.imap_server = server
