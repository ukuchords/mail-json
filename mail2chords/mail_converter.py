import base64
import json
import os
from chord_pro_song import Song

class MailObject:

    def __init__(self, from_text, subject_text, payload_text):

        self.from_text = from_text
        self.subject_text = subject_text
        self.payload_text = payload_text


class MailConverter:

    def __decode_base64_string(self, string2convert):

        return base64.b64decode(string2convert).decode() if string2convert is not None else ""
    def __get_plain_text_part(self, parts):

        payload_text = ""
        for part in parts:
            content_type = part['content_type']
            if content_type == 'text/plain':
                payload = part['payload']
                payload_text = base64.b64decode(payload.encode()).decode("utf-8")
        return payload_text

    def mail_json2mail_object(self, json_string):

        mail_objects = []
        mail_list = json.loads(json_string)
        for mail in mail_list:
            subject_text = self.__decode_base64_string(mail['subject'])
            from_text = self.__decode_base64_string(mail['from'])
            payload_text = self.__get_plain_text_part(mail['parts'])
            mail_objects.append(MailObject(from_text, subject_text, payload_text))
        return mail_objects

    def mail_object2songs(self, json_string):

        songs = []
        mail_objects = self.mail_json2mail_object(json_string)
        for mail_object in mail_objects:
            songs.append(Song(mail_object.payload_text))
        return songs

    def mail_object2song_files(self, mail2json_string):

        print("mail_object2song_files")
        mail_objects = self.mail_json2mail_object(mail2json_string)
        for mail_object in mail_objects:
            song = Song(mail_object.payload_text)
            song_title = song.metadata.get("title")
            song_artist = song.metadata.get("artist")
            if song_title and song_artist and mail_object.payload_text:
                filename = song_title.replace(" ", "_")+".cho"
                file_with_path = os.path.join(self.target_path, filename)
                print(os.path.abspath(file_with_path))
                with open(os.path.abspath(file_with_path), "w",  encoding='utf8', newline='\n') as f:
                    f.write(mail_object.payload_text)

    def __init__(self, target_path):
        self.target_path = target_path
        os.makedirs(target_path, exist_ok=True)
        print("init")
