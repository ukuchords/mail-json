from unittest import TestCase
from chord_pro_song import Song

class TestSong(TestCase):
    def test_parse_example(self):
        song_text = '''
        # A simple ChordPro song.

        {title: Swing Low Sweet Chariot}
        
        {start_of_chorus}
        Swing [D]low, sweet [G]chari[D]ot,
        Comin’ for to carry me [A7]home.
        Swing [D7]low, sweet [G]chari[D]ot,
        Comin’ for to [A7]carry me [D]home.
        {end_of_chorus}
        
        I [D]looked over Jordan, and [G]what did I [D]see,
        Comin’ for to carry me [A7]home.
        A [D]band of angels [G]comin’ after [D]me,
        Comin’ for to [A7]carry me [D]home.
        
        {comment: Chorus}
        '''

        parsed_song= Song(song_text)

        self.assertEqual(parsed_song.metadata.get("title"), 'Swing Low Sweet Chariot')

    def test_parse_directive(self):
        song_text = '''
        {new_song}
          {title: The Last Farwell}
        {sorttitle: Last Farwell, The}
        {subtitle: 2nd Movement}
        {artist: Leonard Cohen}
        {composer: Leonard Cohen}
        {lyricist: Leonard Nijgh}
        {copyright: 2014 Shitting Bull Inc.}
        {album: Songs Of Love And Hate}
        {year: 2016}
        {key: C}
        {time: 4/4}
        {tempo: 120}
        {duration: 268} 
        {capo: 2}
        {chorus}
        {chorus: Final}
        {define: Bes base-fret 1 frets 1 1 3 3 3 1 fingers 1 1 2 3 4 1}
        {chord: Am}
        {chord: Bes base-fret 1 frets 1 1 3 3 3 1 fingers 1 1 2 3 4 1}
        {chord: As  base-fret 4 frets 1 3 3 2 1 1 fingers 1 3 4 2 1 1}
        {start_of_chorus}
        Swing [D]low, sweet [G]chari[D]ot,
        Comin’ for to carry me [A7]home.
        Swing [D7]low, sweet [G]chari[D]ot,
        Comin’ for to [A7]carry me [D]home.
        {end_of_chorus}
        
        I [D]looked over Jordan, and [G]what did I [D]see,
        {x_cord_times: 2 4 3}
        Comin’ for to carry me [A7]home.
        '''

        parsed_song= Song(song_text)

        self.assertEqual(parsed_song.metadata.get("title"), 'The Last Farwell')
        self.assertEqual(parsed_song.metadata.get("artist"), 'Leonard Cohen')