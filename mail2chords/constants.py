# from https://git.snyman.info/raoul/igitar
import re

KNOWN_DIRECTIVES = [
    ('title', 't'),
    ('subtitle', 'st'),
    ('artist',),
    ('composer',),
    ('lyricist',),
    ('arranger',),
    ('copyright',),
    ('album',),
    ('year',),
    ('key',),
    ('time',),
    ('tempo',),
    ('duration',),
    ('capo',),
    ('meta',),
    ('comment', 'c')
]
KNOWN_VERSE_TYPES = [
    'verse',
    'chorus',
    'bridge',
    'tab',
    'grid'
]
ENGLISH_NOTES = '[CDEFGAB]'
GERMAN_NOTES = '[CDEFGAH]'
NEOLATIN_NOTES = '(Do|Re|Mi|Fa|Sol|La|Si)'
CHORD_SUFFIXES = '(b|bb)?(#)?(m|maj7|maj|min7|min|sus)?(1|2|3|4|5|6|7|8|9)?'
SLIM_CHARS = 'fiíIÍjlĺľrtť.,;/ ()|"\'!:\\'
DIRECTIVE = re.compile(r'\{(' + '|'.join([d for t in KNOWN_DIRECTIVES for d in t]) + r'): *(.*?)\}')
START_OF = re.compile(r'\{start_of_(' + '|'.join(KNOWN_VERSE_TYPES) + r')(: *(.*?))?\}')
END_OF = re.compile(r'\{end_of_(' + '|'.join(KNOWN_VERSE_TYPES) + r')\}')
CHORUS_MARKER = re.compile(r'\{chorus(: *(.*?))?\}')
VERSE_MARKER = re.compile(r'\{verse(: *(.*?))?\}')
BRIDGE_MARKER = re.compile(r'\{bridge(: *(.*?))?\}')
CHORD_WORD = re.compile(r'(.*?)\[(.*?)\]')
CHORD = re.compile(r'\[(.*?)\]')
