from unittest import TestCase
from mail_converter import MailConverter
import os

class TestMailConverter(TestCase):
    def test_main2chords(self):

        json_string =  '''
        [
          {
            "subject": "VGVzdG1haWw=",
            "from": "UmFpbmVyIFNjaG5laWRlciA8cmFpbmVyQG9ndi1zY2hpZXNzZW4uZGU+",
            "parts": [
              {
                "level": "part",
                "content_type": "text/plain",
                "payload": "VGVzdGluaGFsdA0K",
                "filename": ""
              },
              {
                "level": "part",
                "content_type": "text/html",
                "payload": "PCFET0NUWVBFIGh0bWw+DQo8aHRtbCBkYXRhLWx0LWluc3RhbGxlZD0idHJ1ZSI+DQogIDxoZWFkPg0KDQogICAgPG1ldGEgaHR0cC1lcXVpdj0iY29udGVudC10eXBlIiBjb250ZW50PSJ0ZXh0L2h0bWw7IGNoYXJzZXQ9VVRGLTgiPg0KICA8L2hlYWQ+DQogIDxib2R5IHN0eWxlPSJwYWRkaW5nLWJvdHRvbTogMXB4OyI+DQogICAgPHA+VGVzdGluaGFsdDxicj4NCiAgICA8L3A+DQogIDwvYm9keT4NCiAgPGx0LWNvbnRhaW5lcj48L2x0LWNvbnRhaW5lcj4NCjwvaHRtbD4NCg==",
                "filename": ""
              }
            ]
          },
          {
            "subject": "Y2hvcmRwcm8=",
            "from": "UmFpbmVyIFNjaG5laWRlciA8cmFpbmVyQG9ndi1zY2hpZXNzZW4uZGU+",
            "parts": [
              {
                "level": "msg",
                "content_type": "text/plain",
                "payload": "U3dpbmcgW0RdbG93LCBzd2VldCBbR11jaGFyaVtEXW90LA0KQ29taW7igJkgZm9yIHRvIGNhcnJ5IG1lIFtBN11ob21lLg0KU3dpbmcgW0Q3XWxvdywgc3dlZXQgW0ddY2hhcmlbRF1vdCwNCkNvbWlu4oCZIGZvciB0byBbQTddY2FycnkgbWUgW0RdaG9tZS4NCg==",
                "filename": ""
              }
            ]
          }
        ]
                '''

        reader = MailConverter("test")
        mail_objects = reader.mail_json2mail_object(json_string)
        self.assertEqual(mail_objects[0].from_text, 'Rainer Schneider <rainer@ogv-schiessen.de>')
        self.assertEqual(mail_objects[0].subject_text, 'Testmail')
        self.assertEqual(mail_objects[0].payload_text, 'Testinhalt\r\n')

    def test_main2sogs(self):

        json_string =  '''
        [
          {
            "subject": "VGVzdG1haWw=",
            "from": "UmFpbmVyIFNjaG5laWRlciA8cmFpbmVyQG9ndi1zY2hpZXNzZW4uZGU+",
            "parts": [
              {
                "level": "part",
                "content_type": "text/plain",
                "payload": "VGVzdGluaGFsdA0K",
                "filename": ""
              },
              {
                "level": "part",
                "content_type": "text/html",
                "payload": "PCFET0NUWVBFIGh0bWw+DQo8aHRtbCBkYXRhLWx0LWluc3RhbGxlZD0idHJ1ZSI+DQogIDxoZWFkPg0KDQogICAgPG1ldGEgaHR0cC1lcXVpdj0iY29udGVudC10eXBlIiBjb250ZW50PSJ0ZXh0L2h0bWw7IGNoYXJzZXQ9VVRGLTgiPg0KICA8L2hlYWQ+DQogIDxib2R5IHN0eWxlPSJwYWRkaW5nLWJvdHRvbTogMXB4OyI+DQogICAgPHA+VGVzdGluaGFsdDxicj4NCiAgICA8L3A+DQogIDwvYm9keT4NCiAgPGx0LWNvbnRhaW5lcj48L2x0LWNvbnRhaW5lcj4NCjwvaHRtbD4NCg==",
                "filename": ""
              }
            ]
          },
          {
            "subject": "Y2hvcmRwcm8=",
            "from": "UmFpbmVyIFNjaG5laWRlciA8cmFpbmVyQG9ndi1zY2hpZXNzZW4uZGU+",
            "parts": [
              {
                "level": "msg",
                "content_type": "text/plain",
                "payload": "U3dpbmcgW0RdbG93LCBzd2VldCBbR11jaGFyaVtEXW90LA0KQ29taW7igJkgZm9yIHRvIGNhcnJ5IG1lIFtBN11ob21lLg0KU3dpbmcgW0Q3XWxvdywgc3dlZXQgW0ddY2hhcmlbRF1vdCwNCkNvbWlu4oCZIGZvciB0byBbQTddY2FycnkgbWUgW0RdaG9tZS4NCg==",
                "filename": ""
              }
            ]
          }
        ]
                '''

        reader = MailConverter("test")
        songs = reader.mail_object2songs(json_string)
        self.assertEqual(songs[0].metadata.get("title"), None)
        self.assertEqual(songs[0].metadata.get("artist"), None)

    def test_main2sogs_files(self):

        json_string = '''
        [
          {
            "subject": "U3dpbmcgTG93",
            "from": "UmFpbmVyIFNjaG5laWRlciA8cmFpbmVyQG9ndi1zY2hpZXNzZW4uZGU+",
            "parts": [
              {
                "level": "msg",
                "content_type": "text/plain",
                "payload": "IyBBIHNpbXBsZSBDaG9yZFBybyBzb25nLg0KDQp7dGl0bGU6IFN3aW5nIExvdyBTd2VldCBDaGFyaW90fQ0Ke2FydGlzdDogTGVvbmFyZCBDb2hlbn0NCg0Ke3N0YXJ0X29mX2Nob3J1c30NClN3aW5nIFtEXWxvdywgc3dlZXQgW0ddY2hhcmlbRF1vdCwNCkNvbWlu4oCZIGZvciB0byBjYXJyeSBtZSBbQTddaG9tZS4NClN3aW5nIFtEN11sb3csIHN3ZWV0IFtHXWNoYXJpW0Rdb3QsDQpDb21pbuKAmSBmb3IgdG8gW0E3XWNhcnJ5IG1lIFtEXWhvbWUuDQp7ZW5kX29mX2Nob3J1c30NCg0KSSBbRF1sb29rZWQgb3ZlciBKb3JkYW4sIGFuZCBbR113aGF0IGRpZCBJIFtEXXNlZSwNCkNvbWlu4oCZIGZvciB0byBjYXJyeSBtZSBbQTddaG9tZS4NCkEgW0RdYmFuZCBvZiBhbmdlbHMgW0ddY29taW7igJkgYWZ0ZXIgW0RdbWUsDQpDb21pbuKAmSBmb3IgdG8gW0E3XWNhcnJ5IG1lIFtEXWhvbWUuDQoNCntjb21tZW50OiBDaG9ydXN9DQo=",
                "filename": ""
              }
            ]
          }
        ]
        '''

        reader = MailConverter("./test")
        reader.mail_object2song_files(json_string)

    def read_from_file(self, file2read_name):

        print(os.path.abspath(file2read_name))
        with open(file2read_name, 'r', encoding="utf-16") as file2read:
            content = file2read.read()
        return content

    def test_read_file(self):
        self.read_from_file("../mailtest.json")
