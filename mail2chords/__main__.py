import os
import sys
import argparse

from mail_converter import MailConverter


def read_from_stdin():
    stdin_text = ''
    for line in sys.stdin:
        stdin_text = stdin_text + line
    return stdin_text


def read_from_file(file2read_name):

    print(os.path.abspath(file2read_name))
    with open(file2read_name, 'r', encoding="utf-8") as file2read:
        content = file2read.read()
    return content


def main() -> int:

    parser = argparse.ArgumentParser(prog='mail2songs',
                                     description='Read mail json form stdin  '
                                                 'and write chordpro songfiles inot directory ')
    parser.add_argument('--targetdir', nargs=1, required=True, help='Directory to write song filed')
    parser.add_argument('--mail_json_file', nargs=1, required=False, help='Mail2Json file to read songs from')

    args = parser.parse_args()
    target_dir = args.targetdir[0]
    mail_json_file = args.mail_json_file[0]
    print("TargetDir: " + target_dir)
    print("Mail Json File: " + mail_json_file)
    converter = MailConverter(target_dir)
    input_mail2json = read_from_file(mail_json_file) if mail_json_file else read_from_stdin()
    print("Text: " + input_mail2json)
    converter.mail_object2song_files(input_mail2json)

    return 0


if __name__ == "__main__":
    sys.exit(main())
