#Deriving the latest base image
FROM python:latest

#Labels as key value pair
LABEL Maintainer="schnurlei@gmail.com"

WORKDIR /usr/app/src

#to COPY the remote file at working directory in container
COPY mailjson /usr/app/src/mailjson
COPY mail2chords /usr/app/src/mail2chords
COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

#CMD instruction should be used to run the software
#contained by your image, along with any arguments.

CMD [ "bash"]